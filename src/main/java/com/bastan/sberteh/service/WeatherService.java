package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Weather;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface WeatherService {

    Weather getCurrentWeather() throws IOException;

    List<Weather> getArchivalWeather(Date from, Date to);
}
