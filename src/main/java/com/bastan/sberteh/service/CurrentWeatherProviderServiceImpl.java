package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Weather;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URL;
import org.springframework.stereotype.Service;

@Service
public class CurrentWeatherProviderServiceImpl implements CurrentWeatherProviderService {

    @Override
    public Weather getCurrentWaetherData() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("http://api.openweathermap.org/data/2.5/weather?id=1486209&APPID=88e69f0b55882a2cdd3ac4362b9eb998&units=metric");
        return mapper.readValue(url, Weather.class);
    }

}
