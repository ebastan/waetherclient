package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Rate;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface RateService {

    Rate getCurrentRate() throws IOException;

    List<Rate> getArchivalRate(Date from, Date to);
}
