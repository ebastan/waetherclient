package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Weather;
import com.bastan.sberteh.repository.WeatherRepository;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Autowired
    private WeatherRepository repository;
    @Autowired
    private CurrentWeatherProviderService currentWaetherService;

    @Override
    public Weather getCurrentWeather() throws IOException {
        Weather curWaether = currentWaetherService.getCurrentWaetherData();
        return repository.saveAndFlush(curWaether);
    }

    @Override
    public List<Weather> getArchivalWeather(Date from, Date to) {
        return repository.findByPublishedBetween(from, to);
    }

}
