package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Weather;
import java.io.IOException;

public interface CurrentWeatherProviderService {

    public Weather getCurrentWaetherData() throws IOException;
}
