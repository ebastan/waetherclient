package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Rate;
import com.bastan.sberteh.repository.RateRepository;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RateServiceImpl implements RateService {

    @Autowired
    private RateRepository repository;
    @Autowired
    private CurrentRateProviderService currentRateService;

    @Override
    public Rate getCurrentRate() throws IOException {
        Rate curRate = currentRateService.getCurrentRateData();
        return repository.saveAndFlush(curRate);
    }

    @Override
    public List<Rate> getArchivalRate(Date from, Date to) {
        return repository.findByPublishedBetween(from, to);
    }

}
