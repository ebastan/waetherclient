package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Rate;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URL;
import org.springframework.stereotype.Service;

@Service
public class CurrentRateProviderServiceImpl implements CurrentRateProviderService {

    @Override
    public Rate getCurrentRateData() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        URL url = new URL("https://query.yahooapis.com/v1/public/yql?q=select+*+from+yahoo.finance.xchange+where+pair+=+%22USDRUB,EURRUB,EURUSD%22&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
        return mapper.readValue(url, Rate.class);
    }

}
