package com.bastan.sberteh.service;

import com.bastan.sberteh.entity.Rate;
import java.io.IOException;

public interface CurrentRateProviderService {

    public Rate getCurrentRateData() throws IOException;
}
