package com.bastan.sberteh.entity;

import com.bastan.sberteh.json.WeatherDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Entity
@Table(name = "weather")
@JsonDeserialize(using = WeatherDeserializer.class)
public class Weather implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "published_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date published;

    private float temp;

    private int pressure;

    private int humidity;

    private String description;

    @Column(name = "city_id")
    private int cityId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return "Weather{" + "id=" + id + ", published=" + published + ", temp=" + temp + ", pressure=" + pressure + ", humidity=" + humidity + ", description=" + description + ", cityId=" + cityId + '}';
    }
}
