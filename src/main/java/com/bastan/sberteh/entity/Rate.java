package com.bastan.sberteh.entity;

import com.bastan.sberteh.json.RateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

@Entity
@Table(name = "rate")
@JsonDeserialize(using = RateDeserializer.class)
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "published_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date published;

    @Column(name = "usdrub")
    private double rateUSDRUB;

    @Column(name = "eurrub")
    private double rateEURRUB;

    @Column(name = "eurusd")
    private double rateEURUSD;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public double getRateUSDRUB() {
        return rateUSDRUB;
    }

    public void setRateUSDRUB(double rateUSDRUB) {
        this.rateUSDRUB = rateUSDRUB;
    }

    public double getRateEURRUB() {
        return rateEURRUB;
    }

    public void setRateEURRUB(double rateEURRUB) {
        this.rateEURRUB = rateEURRUB;
    }

    public double getRateEURUSD() {
        return rateEURUSD;
    }

    public void setRateEURUSD(double rateEURUSD) {
        this.rateEURUSD = rateEURUSD;
    }

    @Override
    public String toString() {
        return "Rate{" + "id=" + id + ", published=" + published + ", rateUSDRUB=" + rateUSDRUB + ", rateEURRUB=" + rateEURRUB + ", rateEURUSD=" + rateEURUSD + '}';
    }

}
