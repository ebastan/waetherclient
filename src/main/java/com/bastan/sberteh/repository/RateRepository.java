package com.bastan.sberteh.repository;

import com.bastan.sberteh.entity.Rate;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RateRepository extends JpaRepository<Rate, Long> {

    public List<Rate> findByPublishedBetween(Date from, Date to);
}
