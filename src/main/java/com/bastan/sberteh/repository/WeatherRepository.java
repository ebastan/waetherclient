package com.bastan.sberteh.repository;

import com.bastan.sberteh.entity.Weather;
import java.util.Date;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherRepository extends JpaRepository<Weather, Long> {

    public List<Weather> findByPublishedBetween(Date from, Date to);
}
