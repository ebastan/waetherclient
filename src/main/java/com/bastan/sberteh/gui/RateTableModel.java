package com.bastan.sberteh.gui;

import com.bastan.sberteh.entity.Rate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class RateTableModel implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<>();

    private List<Rate> raterList = new ArrayList<>();

    public RateTableModel() {
    }

    public RateTableModel(List<Rate> raterList) {
        this.raterList = raterList;
    }

    @Override
    public int getRowCount() {
        return raterList.size();
    }

    @Override
    public int getColumnCount() {
        return 5;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Rate r = raterList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return r.getId();
            case 1:
                return r.getRateEURRUB();
            case 2:
                return r.getRateUSDRUB();
            case 3:
                return r.getRateEURUSD();
            case 4:
                return r.getPublished();
        }
        return "";
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Номер запроса";
            case 1:
                return "EUR/RUB";
            case 2:
                return "USD/RUB";
            case 3:
                return "EUR/USD";
            case 4:
                return "Время запроса";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

}
