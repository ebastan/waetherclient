package com.bastan.sberteh.gui;

import com.bastan.sberteh.entity.Rate;
import com.bastan.sberteh.entity.Weather;
import com.bastan.sberteh.service.RateService;
import com.bastan.sberteh.service.WeatherService;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

public class MainFrame extends javax.swing.JFrame {

    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
    public static final long DEFAULT_UPDATE_PERIOD = 60 * 1000;
    private Weather currentWeather;
    private Rate currentRate;
    private final Timer updateTimer = new Timer("WeatherUpdateTimer");
    private TimerTask updateTimerTask;
    private final WeatherService weatherService;
    private final RateService rateService;
    private long updatePeriod = DEFAULT_UPDATE_PERIOD;
    private WeatherFrame weatherFrame;

    public MainFrame(WeatherService weatherService, RateService rateService) {
        this.weatherService = weatherService;
        this.rateService = rateService;
        initComponents();
        restartWeatherUpdateTimer();
        errorLabel.setVisible(false);
    }

    public void restartWeatherUpdateTimer() {
        if (updateTimerTask != null) {
            updateTimerTask.cancel();
            updateTimerTask = null;
        }
        updateTimerTask = new TimerTask() {
            @Override
            public void run() {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        updateLabel.setVisible(true);
                    }
                });
                try {
                    Weather cw = weatherService.getCurrentWeather();
                    currentWeather = cw;
                } catch (Exception ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    errorLabel.setVisible(true);
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        initWeatherContent();
                        updateLabel.setVisible(false);
                        updateLabel1.setVisible(true);
                    }
                });
                try {
                    Rate cr = rateService.getCurrentRate();
                    currentRate = cr;
                } catch (Exception ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    errorLabel.setVisible(true);
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        initRateContent();
                        updateLabel1.setVisible(false);
                    }
                });
            }
        };
        updateTimer.schedule(updateTimerTask, 0, updatePeriod);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        errorLabel = new javax.swing.JLabel();
        descriptionLabel = new javax.swing.JLabel();
        tempLabel = new javax.swing.JLabel();
        pressureLabel = new javax.swing.JLabel();
        humidityLabel = new javax.swing.JLabel();
        publishedLabel = new javax.swing.JLabel();
        updateLabel = new javax.swing.JLabel();
        rateLabel2 = new javax.swing.JLabel();
        rateLabel1 = new javax.swing.JLabel();
        rateLabel3 = new javax.swing.JLabel();
        publishedLabel1 = new javax.swing.JLabel();
        updateLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu3 = new javax.swing.JMenu();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem2 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem3 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem4 = new javax.swing.JRadioButtonMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jMenuItem1 = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        jMenuItem3 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Главное окно");
        setMaximumSize(new java.awt.Dimension(900, 600));
        setMinimumSize(new java.awt.Dimension(900, 600));
        setPreferredSize(new java.awt.Dimension(900, 600));
        setResizable(false);
        getContentPane().setLayout(null);

        errorLabel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        errorLabel.setText("Типо высокоуровневый ответ на ошибку.");
        getContentPane().add(errorLabel);
        errorLabel.setBounds(20, 220, 750, 100);

        descriptionLabel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        descriptionLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        descriptionLabel.setMaximumSize(new java.awt.Dimension(400, 100));
        descriptionLabel.setMinimumSize(new java.awt.Dimension(400, 100));
        descriptionLabel.setPreferredSize(new java.awt.Dimension(400, 100));
        getContentPane().add(descriptionLabel);
        descriptionLabel.setBounds(30, 20, 400, 100);

        tempLabel.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        tempLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        tempLabel.setMaximumSize(new java.awt.Dimension(400, 100));
        tempLabel.setMinimumSize(new java.awt.Dimension(400, 100));
        tempLabel.setPreferredSize(new java.awt.Dimension(400, 100));
        getContentPane().add(tempLabel);
        tempLabel.setBounds(460, 20, 400, 100);

        pressureLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        pressureLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        pressureLabel.setMaximumSize(new java.awt.Dimension(400, 50));
        pressureLabel.setMinimumSize(new java.awt.Dimension(400, 50));
        pressureLabel.setPreferredSize(new java.awt.Dimension(400, 50));
        getContentPane().add(pressureLabel);
        pressureLabel.setBounds(30, 130, 400, 50);

        humidityLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        humidityLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        humidityLabel.setMaximumSize(new java.awt.Dimension(400, 50));
        humidityLabel.setMinimumSize(new java.awt.Dimension(400, 50));
        humidityLabel.setPreferredSize(new java.awt.Dimension(400, 50));
        getContentPane().add(humidityLabel);
        humidityLabel.setBounds(460, 130, 400, 50);

        publishedLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        publishedLabel.setMaximumSize(new java.awt.Dimension(200, 30));
        publishedLabel.setMinimumSize(new java.awt.Dimension(200, 30));
        publishedLabel.setPreferredSize(new java.awt.Dimension(200, 30));
        getContentPane().add(publishedLabel);
        publishedLabel.setBounds(660, 190, 200, 30);

        updateLabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        updateLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateLabel.setText("Обновляется...");
        updateLabel.setMaximumSize(new java.awt.Dimension(200, 30));
        updateLabel.setMinimumSize(new java.awt.Dimension(200, 30));
        updateLabel.setPreferredSize(new java.awt.Dimension(200, 30));
        getContentPane().add(updateLabel);
        updateLabel.setBounds(460, 190, 200, 30);

        rateLabel2.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        rateLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rateLabel2.setMaximumSize(new java.awt.Dimension(400, 100));
        rateLabel2.setMinimumSize(new java.awt.Dimension(400, 100));
        rateLabel2.setPreferredSize(new java.awt.Dimension(400, 100));
        getContentPane().add(rateLabel2);
        rateLabel2.setBounds(460, 300, 400, 100);

        rateLabel1.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        rateLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rateLabel1.setMaximumSize(new java.awt.Dimension(400, 100));
        rateLabel1.setMinimumSize(new java.awt.Dimension(400, 100));
        rateLabel1.setPreferredSize(new java.awt.Dimension(400, 100));
        getContentPane().add(rateLabel1);
        rateLabel1.setBounds(30, 300, 400, 100);

        rateLabel3.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        rateLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rateLabel3.setMaximumSize(new java.awt.Dimension(400, 100));
        rateLabel3.setMinimumSize(new java.awt.Dimension(400, 100));
        rateLabel3.setPreferredSize(new java.awt.Dimension(400, 100));
        getContentPane().add(rateLabel3);
        rateLabel3.setBounds(30, 420, 400, 100);

        publishedLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        publishedLabel1.setMaximumSize(new java.awt.Dimension(200, 30));
        publishedLabel1.setMinimumSize(new java.awt.Dimension(200, 30));
        publishedLabel1.setPreferredSize(new java.awt.Dimension(200, 30));
        getContentPane().add(publishedLabel1);
        publishedLabel1.setBounds(650, 490, 200, 30);

        updateLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        updateLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateLabel1.setText("Обновляется...");
        updateLabel1.setMaximumSize(new java.awt.Dimension(200, 30));
        updateLabel1.setMinimumSize(new java.awt.Dimension(200, 30));
        updateLabel1.setPreferredSize(new java.awt.Dimension(200, 30));
        getContentPane().add(updateLabel1);
        updateLabel1.setBounds(460, 420, 200, 30);

        jMenu1.setText("Меню");
        jMenu1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jMenu3.setText("Частота обновления");

        buttonGroup1.add(jRadioButtonMenuItem1);
        jRadioButtonMenuItem1.setText("1 секунда");
        jRadioButtonMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem1ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem1);

        buttonGroup1.add(jRadioButtonMenuItem2);
        jRadioButtonMenuItem2.setText("5 секунд");
        jRadioButtonMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem2ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem2);

        buttonGroup1.add(jRadioButtonMenuItem3);
        jRadioButtonMenuItem3.setSelected(true);
        jRadioButtonMenuItem3.setText("1 минута");
        jRadioButtonMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem3ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem3);

        buttonGroup1.add(jRadioButtonMenuItem4);
        jRadioButtonMenuItem4.setText("10 минут");
        jRadioButtonMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem4ActionPerformed(evt);
            }
        });
        jMenu3.add(jRadioButtonMenuItem4);

        jMenu1.add(jMenu3);
        jMenu1.add(jSeparator1);

        jMenuItem1.setText("История запросов");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);
        jMenu1.add(jSeparator2);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Выход");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jRadioButtonMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem1ActionPerformed
        setWeatherUpdatePeriod(1000);
        restartWeatherUpdateTimer();
    }//GEN-LAST:event_jRadioButtonMenuItem1ActionPerformed

    private void jRadioButtonMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem2ActionPerformed
        setWeatherUpdatePeriod(5 * 1000);
        restartWeatherUpdateTimer();
    }//GEN-LAST:event_jRadioButtonMenuItem2ActionPerformed

    private void jRadioButtonMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem3ActionPerformed
        setWeatherUpdatePeriod(60 * 1000);
        restartWeatherUpdateTimer();
    }//GEN-LAST:event_jRadioButtonMenuItem3ActionPerformed

    private void jRadioButtonMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem4ActionPerformed
        setWeatherUpdatePeriod(10 * 60 * 1000);
        restartWeatherUpdateTimer();
    }//GEN-LAST:event_jRadioButtonMenuItem4ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        showWeatherFrame();        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void initWeatherContent() {
        if (currentWeather != null) {
            descriptionLabel.setText(currentWeather.getDescription());
            tempLabel.setText("Температура " + currentWeather.getTemp() + "°С");
            pressureLabel.setText("Давление " + currentWeather.getPressure());
            humidityLabel.setText("Влажность " + currentWeather.getHumidity() + "%");
            publishedLabel.setText(DATE_FORMAT.format(currentWeather.getPublished()));
        }
    }

    private void initRateContent() {
        if (currentRate != null) {
            rateLabel1.setText("USD/RUB:" + currentRate.getRateUSDRUB());
            rateLabel2.setText("EUR/RUB:" + currentRate.getRateEURRUB());
            rateLabel3.setText("EUR/USD:" + currentRate.getRateEURUSD());
            publishedLabel1.setText(DATE_FORMAT.format(currentRate.getPublished()));
        }
    }

    public void setWeatherUpdatePeriod(long weatherUpdatePeriod) {
        this.updatePeriod = weatherUpdatePeriod;
    }

    public void showWeatherFrame() {
        if (weatherFrame == null) {
            weatherFrame = new WeatherFrame(weatherService, rateService);
        }
        weatherFrame.setVisible(true);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JLabel errorLabel;
    private javax.swing.JLabel humidityLabel;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem2;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem3;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem4;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel pressureLabel;
    private javax.swing.JLabel publishedLabel;
    private javax.swing.JLabel publishedLabel1;
    private javax.swing.JLabel rateLabel1;
    private javax.swing.JLabel rateLabel2;
    private javax.swing.JLabel rateLabel3;
    private javax.swing.JLabel tempLabel;
    private javax.swing.JLabel updateLabel;
    private javax.swing.JLabel updateLabel1;
    // End of variables declaration//GEN-END:variables
}
