package com.bastan.sberteh.gui;

import com.bastan.sberteh.entity.Weather;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class WeatherTableModel implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<>();

    private List<Weather> weatherList = new ArrayList<>();

    public WeatherTableModel() {
    }

    public WeatherTableModel(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }

    @Override
    public int getRowCount() {
        return weatherList.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Weather w = weatherList.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return w.getId();
            case 1:
                return w.getDescription();
            case 2:
                return w.getTemp();
            case 3:
                return w.getPressure();
            case 4:
                return w.getHumidity();
            case 5:
                return w.getPublished();
        }
        return "";
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Номер запроса";
            case 1:
                return "Описание";
            case 2:
                return "Температура";
            case 3:
                return "Давление";
            case 4:
                return "Влажность";
            case 5:
                return "Время запроса";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
        listeners.add(l);
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
        listeners.remove(l);
    }

}
