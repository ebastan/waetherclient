package com.bastan.sberteh.json;

import com.bastan.sberteh.entity.Rate;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;

public class RateDeserializer extends StdDeserializer<Rate> {

    public RateDeserializer() {
        this(null);
    }

    public RateDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Rate deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        JsonNode rate = node.findPath("rate");
        Iterator<JsonNode> elements = rate.elements();
        double USDRUB = 1;
        double EURRUB = 2;
        double EURUSD = 3;
        while (elements.hasNext()) {
            JsonNode next = elements.next();
            String textValue = next.get("id").textValue();
            double floatValue = next.get("Rate").asDouble();
            switch (textValue) {
                case "USDRUB":
                    USDRUB = floatValue;
                    break;
                case "EURRUB":
                    EURRUB = floatValue;
                    break;
                case "EURUSD":
                    EURUSD = floatValue;
                    break;
            }
        }
        Rate r = new Rate();
        r.setRateEURRUB(EURRUB);
        r.setRateEURUSD(EURUSD);
        r.setRateUSDRUB(USDRUB);
        r.setPublished(new Date());

        return r;
    }

}
