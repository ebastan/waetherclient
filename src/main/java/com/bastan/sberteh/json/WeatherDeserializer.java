package com.bastan.sberteh.json;

import com.bastan.sberteh.entity.Weather;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;
import java.io.IOException;
import java.util.Date;

public class WeatherDeserializer extends StdDeserializer<Weather> {

    public WeatherDeserializer() {
        this(null);
    }

    public WeatherDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Weather deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        int cityId = ((IntNode) node.get("id")).intValue();
        JsonNode mainNode = node.get("main");
        float temp = (mainNode.get("temp")).floatValue();
        int pressure = ((IntNode) mainNode.get("pressure")).intValue();
        int humidity = ((IntNode) mainNode.get("humidity")).intValue();
        String description = node.findPath("description").asText();
        Weather weather = new Weather();
        weather.setCityId(cityId);
        weather.setDescription(description);
        weather.setHumidity(humidity);
        weather.setPressure(pressure);
        weather.setTemp(temp);
        weather.setPublished(new Date());

        return weather;
    }

}
